import unittest
from replicator.cron import Cron 

class testCron(unittest.TestCase):
    
    cron = None

    def setUp(self):
        self.cron = Cron()
    
    def testAddTasks(self):
        self.assertEqual(self.cron.fronta, {})
        
        #Pokus o pridani nepodporovaneho ukolu.
        self.cron.taskAdd('pepa', 5)
        self.assertEqual(self.cron.fronta, {})
        
        #Pridani ulohy
        self.cron.taskAdd('helloWorld', 5)
        self.assertEqual(self.cron.fronta, {'helloWorld': {'nextRun': 0, 'period': 5}})
        
        #Prepsani existujici ulohy
        self.cron.taskAdd('helloWorld', 10)
        self.assertEqual(self.cron.fronta, {'helloWorld': {'nextRun': 0, 'period': 10}})

    def testGetTasks(self):
        self.cron.taskAdd('helloWorld', 5)
        
        self.assertEqual(self.cron.getTasks(), ['helloWorld']);
        self.cron.taskOk('helloWorld')
        self.assertEqual(self.cron.getTasks(), []);

    def testDelTasks(self):
        self.cron.taskAdd('helloWorld', 5)
        self.assertEqual(self.cron.fronta, {'helloWorld': {'nextRun': 0, 'period': 5}})
        self.cron.taskDel('helloWorld')
        self.cron.taskOk('helloWorld')
        self.assertEqual(self.cron.fronta, {})
