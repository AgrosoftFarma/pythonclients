#!/usr/bin/env python3

import unittest
import sys
from test_replicator.test_cron import testCron

if __name__ == '__main__':
    sys.path.append('../src')

    testsuite = unittest.TestLoader().discover('.')
    unittest.TextTestRunner(verbosity=2).run(testsuite)
    testsuite = unittest.TestLoader().loadTestsFromTestCase(testCron)
    unittest.TextTestRunner(verbosity=2).run(testsuite)
