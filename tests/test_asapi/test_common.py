'''
Created on 16. 6. 2017

@author: krupkaf
'''
import asapi.common
from . import common

class testCommon(common.AsaptiTestCase):

    common = None

    def setUp(self):
        self.common = asapi.common.Common();
        self.common.setLogin(
            self.confGet('ASApiServer', 'url'),
            self.confGet('ASApiServer', 'userName'),
            self.confGet('ASApiServer', 'userPassword'),
            self.confGet('ASApiServer', 'appName'),
            self.confGet('ASApiServer', 'companyId'))

    def tearDown(self):
        pass


    def test10Query(self):
        res = self.common.query('user.isLogged', {"testUserUid":"4848d8fd3b237b8b52d5a25bdc114c9b"});
        self.assertEqual('Not logged.', res)

    def test11Query(self):
        with self.assertRaises(asapi.common.ASapiException) as cm:
            self.common.query('aaaa', None);

        exc = cm.exception
        self.assertEqual(-32601, exc.code)
        self.assertEqual('Nepodporovaná funkce!', exc.msg)

    def test21Login(self):
        self.common.setLogin(
            self.confGet('ASApiServer', 'url'),
            'sssss',
            'ddddd',
            self.confGet('ASApiServer', 'appName'),
            self.confGet('ASApiServer', 'companyId'))
        with self.assertRaises(asapi.common.ASapiException) as cm:
            self.common.login()
            
        exc = cm.exception
        self.assertEqual(0, exc.code)
        self.assertEqual('Nesprávné uživatelské jméno nebo heslo!', exc.msg)
            
    def test22Login(self):
        self.common.login()
        self.assertTrue(self.common.isLogged(False))
        self.common.logout()
        
    def test31Login(self):
        self.common.login()
        self.assertTrue(self.common.isLogged())
        self.assertTrue(self.common.isLogged(False))
        asapi.common.Common.userUid = 'sssssssssssss'
        self.assertTrue(self.common.isLogged())
        self.assertFalse(self.common.isLogged(False))
        
    def test32Logout(self):
        '''
        Pokus o odhlaseni bez provedeni prihlaseni.
        Provede se nejprve automaticke prihlaseni.
        '''
        self.common.logout()
        
    def test33Logout(self):
        '''
        Pokus o odhlaseni s neplatnym userUid.
        Po zjisteni, ze je neplatne userUid, provede se prihlaseni a pak pozadovane odhlaseni.
        '''        
        asapi.common.Common.userUid = 'sss'
        self.common.logout()
     
        
        
        
        