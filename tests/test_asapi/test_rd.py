'''
Created on 24. 6. 2017

@author: krupkaf
'''

from . import common
import asapi.rd
import json

class testStore(common.AsaptiTestCase):

    rd = None

    def setUp(self):
        self.rd = asapi.rd.RD()
        self.loadLogin()

    def test1(self):
        data = self.rd.fetch('admin.modules', 'admin.modules')
        self.assertIsInstance(data['items'], list)

    def test2(self):
        with self.assertRaises(asapi.common.ASapiException) as cm:
            self.rd.fetch('admin.modules', 'a')
        exc = cm.exception
        self.assertEqual(0, exc.code)
        self.assertEqual('Šablona "a" pro sestavu "admin.modules" není podporovaná!', exc.msg)

    def test3(self):
        #Pokus o stazeni sestavy v csv s pokusem zpracovat to jako json
        #with self.assertRaises(json.decoder.JSONDecodeError) as cm:
        with self.assertRaises(json.decoder.JSONDecodeError) as cm:
            self.rd.fetch('admin.modules', 'csv')
        exc = cm.exception
        self.assertEqual('Invalid control character at', exc.msg)

    def test20(self):
        data = self.rd.download('admin.modules', 'csv')
        self.assertEqual('"Kód";"Jméno";"Úroveň"', data.decode('utf8').split("\n")[3])
