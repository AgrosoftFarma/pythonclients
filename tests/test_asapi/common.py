'''
Created on 19. 6. 2017

@author: krupkaf
'''
import unittest
import configparser
import os
import asapi.common


class AsaptiTestCase(unittest.TestCase):
    config = None
    def __init__(self, methodName='runTest'):
        self.config = configparser.ConfigParser()
        self.config.read(os.path.dirname(os.path.abspath(__file__)) + '/../../config/test.conf')
        super().__init__(methodName)
        
    def confGet(self, section, option):
        return self.config.get(section, option)

    def loadLogin(self):
        '''
        Nacte konfiguraci serveru z konfigu.
        '''
        a = asapi.common.Common() 
        a.setLogin(
            self.confGet('ASApiServer', 'url'),
            self.confGet('ASApiServer', 'userName'),
            self.confGet('ASApiServer', 'userPassword'),
            self.confGet('ASApiServer', 'appName'),
            self.confGet('ASApiServer', 'companyId'))
