# Obsluha vahy

Obsluha vahy. Data uklada do lokalni sqlite databaze odkud si je bere [Replicator](https://gitlab.com/Skotsoft/PythonClients/tree/master/src/replicator) a predava na server.
Scale si sam neumi zalozit DB. DB musi predem vytvorit replicator.

## Instalace

* Pro scale je vhodne pouzit samostatneho uzivatele - agrosoft.
* Adresar src/scale se zkopiruje do /opt/agrosoft/scale
* Veskere soubory *.py a prislusne adresara by mel vlastnit uzivatel agrosoft.
* Zavislosti
    * [libmodbusdevicesPy](https://gitlab.com/AgrosoftModBus/libmodbusdevicesPy)
        * Zkopirovat adresar libmodbusdevicesPy projektu libmodbusdevicesPy do /opt/agrosoft/libmodbusdevicesPy
* Priklad konfigurace [scale.conf.example](https://gitlab.com/Skotsoft/PythonClients/blob/master/src/config/scale.conf.example)
    * Konfigurace se ulozi do souboru /opt/agrosoft/config/scale.conf
* Spusteni scale jako sluzbu pod systemd
    * Zkopirovat (dle potrebi upravit) soubor [systemd/ascale.service](https://gitlab.com/Skotsoft/PythonClients/blob/master/systemd/ascale.service) do /etc/systemd/system/. Upravit opravneni na 0755 a vlastnika na root:root
    * Povoleni sluzby: `systemctl enable ascale.service`
    * Rucni spusteni: `systemctl start ascale.service`

## Log
Scale vypisuje log na konzoli. Pokud je Scale spusteny jako sluzba systemd, systemd logy odchytava a standardne zpracovava.
* `systemctl status ascale.service`
* `journalctl --unit=ascale.service`
