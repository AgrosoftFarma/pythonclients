#!/usr/bin/env python3

from driverIdentificationAbstract import driverIdentificationAbstract
import serial

class driverIdentificationHCS301(driverIdentificationAbstract):
    '''
    Trida identifikace s ovladaci HCS301.
    '''
    
    portName = None
    
    serialPort = None
    
    aPlaceId = None
    
    def __init__(self, port):
        self.portName = port
        self.openPort()
        pass

    def openPort(self):
        self.serialPort = serial.Serial(self.portName, 115200, timeout=0.2)

    def getIdentification(self):
        self.aPlaceId = None
        self.serialPort.flushInput()
        self.serialPort.write(b'5')
        line = self.serialPort.readline()
        if (line == None) or (line == b'None\r\n')  or (line == b'')  or (line == b'\r\n'):
            return None
        
        items = line.split(b';')
        if len(items) != 8:
            #Nedostal jsem predpokladany pocet polozek.
            return None;
        
        if items[7] != b'T\r\n':
            #Ovladac neni overen podle klice
            return None;
        
        serial = items[1].decode("ascii")#Seriove cislo ovladace.
        
        buttons = int(items[2].decode("ascii"), 16)
        if buttons & 0x01:
            self.aPlaceId = self.placeIds[0]
            
        if buttons & 0x02:
            self.aPlaceId = self.placeIds[1]
            
        if buttons & 0x04:
            self.aPlaceId = self.placeIds[2]
        
        if buttons & 0x08:
            self.aPlaceId = self.placeIds[3]
        
        return serial

    def getIdentifikationType(self):
        return 'hcs301'

    def semaphore(self, state):
        if state == True:
            self.serialPort.write(b'6')
        else:
            self.serialPort.write(b'7')
        self.serialPort.readline()

    def getPlaceId(self):
        return self.aPlaceId
    
    def clean(self):
        self.serialPort.flushInput()
        self.serialPort.write(b'5')
        self.serialPort.readline()

if __name__ == "__main__":
    import time
    
    identification = driverIdentificationHCS301('/dev/ttyUSB0')
    identification.setPlaceId([1, 2, 3, 4])
    identification.clean()
    while True:
        id = identification.getIdentification()
        if id != None:
            placeId = identification.getPlaceId()
            print(str(id) + '  ' + str(placeId) + '  ' + identification.getIdentifikationType())
            identification.semaphore(True)
            time.sleep(2)
            identification.semaphore(False)
            identification.getIdentification();
        else:
            print(None)
        time.sleep(0.05)
