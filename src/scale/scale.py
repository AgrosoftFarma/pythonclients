#!/usr/bin/env python3
'''
Created on 9. 3. 2018

@author: krupkaf
'''
import traceback
import sqlite3
import signal
from time import sleep
import configparser
import logging
import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../')


loopStop = False


def signal_term_handler(signal, frame):
    global loopStop
    loopStop = True


def saveWeighing(identifikationType, driver, weight, placeId):
    driver = str(driver).strip()
    driverId = None
    if identifikationType == 'transpnder':
        driverId = sqlS.execute("SELECT id FROM transponders WHERE transponder = upper('" + driver + "') ORDER BY id desc LIMIT 1").fetchone()
    elif identifikationType == 'hcs301':
        driverId = sqlS.execute("SELECT id FROM transponders WHERE hcs301 = '" + driver + "' ORDER BY id desc LIMIT 1").fetchone()

    if driverId:
        driverId = driverId[0]
    else:
        driverId = None

    c = sqlS.cursor()
    c.execute("INSERT INTO wevents(time, place_id, driver_transponder_id, brutto, note) VALUES "
              + "(strftime('%s', 'now'),"
              + str(placeId) + ", "
              + (str(driverId) if driverId is not None else 'NULL') + ", "
              + str(weight) + ","
              + "'" + (("Čip:" + str(driver) + " " + identifikationType) if driverId is None else '') + "')")
    sqlS.commit()


signal.signal(signal.SIGTERM, signal_term_handler)
signal.signal(signal.SIGINT, signal_term_handler)

config = configparser.ConfigParser()
config.read(os.path.dirname(os.path.abspath(__file__)) + '/../config/scale.conf')
logLevel = config.getint('scale', 'logLevel', fallback=40)

logger = logging.getLogger()
logger.setLevel(logLevel)
h = logging.StreamHandler()
h.setLevel(logLevel)
h.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s'))
logger.addHandler(h)

# Pripojeni k DB
sqlS = sqlite3.connect(config.get('db', 'fileName'))

# Inicializace vahy
scale = None
scaleType = config.get('scale', 'scaleType')
if scaleType == 'simulator':
    from scaleSimulator import scaleSimulator
    scale = scaleSimulator()
elif scaleType == 'CI200':
    from scaleCI200 import scaleCI200
    scale = scaleCI200(config.get('scale', 'port'), config.getint('scale', 'nStable', fallback=1))
elif scaleType == 'S20':
    from scaleS20 import scaleS20
    scale = scaleS20(config.get('scale', 'port'), config.getint('scale', 'nStable', fallback=1))
elif scaleType == 'VT200':
    from scaleVT200 import scaleVT200
    scale = scaleVT200(config.get('scale', 'port'), config.getint('scale', 'nStable', fallback=1))
elif scaleType == 'I35':
    from scaleI35 import scaleI35
    scale = scaleI35(config.get('scale', 'url'), config.getint('scale', 'timeout', fallback=1), config.getint('scale', 'maxTest', fallback=1), config.getint('scale', 'nStable', fallback=1))
else:
    logger.error('Chyba konfigurace. Nepodporovany typ vahy. ' + scaleType)
    sys.exit()

if scale is None:
    logger.error('Chyba konfigurace. Není zadán typ váhy.')
    sys.exit()

# Inicializace identifikace ridice
driverIdentification = []
i = 1
while config.has_option('driverIdentification' + str(i), 'type'):
    n = None
    dIdentType = config.get('driverIdentification' + str(i), 'type')
    if dIdentType == 'simulator':
        from driverIdentificationSimulator import driverIdentificationSimulator
        n = driverIdentificationSimulator(sqlS)
    elif dIdentType == 'hcs301':
        from driverIdentificationHCS301 import driverIdentificationHCS301
        n = driverIdentificationHCS301(config.get('driverIdentification' + str(i), 'port'))
    elif dIdentType == 'doorman':
        from driverIdentificationDoorman import driverIdentificationDoorman
        n = driverIdentificationDoorman(config.get('driverIdentification' + str(i), 'port'), config.getint('driverIdentification' + str(i), 'modBusAddress'))
    else:
        logger.error('Chyba konfigurace. Nepodporovany typ identifikace ridice. ' + dIdentType)
        sys.exit()
    n.setPlaceId(config.get('driverIdentification' + str(i), 'placeIds').split(' '))
    driverIdentification.append(n)
    i += 1

if driverIdentification == []:
    logger.error('Chyba konfigurace. Není zadán identifikace ridice.')
    sys.exit()

semaphoreTime = config.getint('scale', 'semaphoreTime')

logger.debug('Start')

try:
    # Prectu identifikaci ze vsech zdroju, abych vyloucil, ze se vezme neco stareho.
    for dIdent in driverIdentification:
        dIdent.clean()
        dIdent.semaphore(False)
    while loopStop is False:
        sleep(0.2)
        for dIdent in driverIdentification:
            driver = dIdent.getIdentification()
            if driver is not None:
                weight = scale.getWeight()
                identifikationType = dIdent.getIdentifikationType()
                placeId = dIdent.getPlaceId()
                logger.info(identifikationType + ' ' + driver + ': ' + str(weight) + ' kg; placeId:' + str(placeId))
                saveWeighing(identifikationType, driver, weight, placeId)
                for dIdent in driverIdentification:
                    dIdent.semaphore(True)
                sleep(semaphoreTime)
                for dIdent in driverIdentification:
                    dIdent.clean()
                    dIdent.semaphore(False)
except BaseException:
    logger.error(traceback.format_exc())

sqlS.close()
logger.debug('End')
