#!/usr/bin/env python3

from driverIdentificationAbstract import driverIdentificationAbstract
import random

class driverIdentificationSimulator(driverIdentificationAbstract):
    '''
    Trida simulace identifikace.
    '''
    def __init__(self, sqlS):
        self.ids = [];
        if sqlS == None:
            for i in range(10):
                self.ids.append(random.randrange(1, 1000))
        else:
            self.ids = sqlS.execute("SELECT group_concat(transponder, ';') FROM transponders WHERE transponder IS NOT NULL").fetchone()[0].split(';')

    def getIdentification(self):
        if random.randrange(0, 10) == 0:            
            return self.ids[random.randrange(0, len(self.ids) - 1)]
        else:
            return None

    def getIdentifikationType(self):
        return 'transpnder'

if __name__ == "__main__":
    import time
    
    identification = driverIdentificationSimulator(None)
    identification.setPlaceId([2])
    while True:
        id = identification.getIdentification();
        if id != None:
            placeId = identification.getPlaceId();
            print(str(id) + '  ' + str(placeId) + '  ' + identification.getIdentifikationType())
            identification.semaphore(True)
            time.sleep(2)
            identification.semaphore(False)
        else:
            print(None)
        time.sleep(0.05)
