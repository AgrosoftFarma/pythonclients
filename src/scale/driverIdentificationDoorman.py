#!/usr/bin/env python3

from driverIdentificationAbstract import driverIdentificationAbstract
import libmodbusdevicesPy.doorman

class driverIdentificationDoorman(driverIdentificationAbstract):
    '''
    Trida identifikace s Doorman_1 (cipy TIRIS).
    '''
    
    portName = None
    
    mbAddress = None
    
    doorman = None
    
    aPlaceId = None
    
    def __init__(self, port, mbAddress):
        self.portName = port
        self.mbAddress = mbAddress
        self.openPort()
        pass

    def openPort(self):
        self.doorman = libmodbusdevicesPy.doorman.Doorman(self.portName, self.mbAddress)

    def getIdentification(self):
        return self.doorman.readTransponder()

    def getIdentifikationType(self):
        return 'transpnder'

    def semaphore(self, state):
        if state == True:
            self.doorman.setRele(1, 1)
        else:
            self.doorman.setRele(1, 0)
    
    def clean(self):
        self.doorman.readTransponder()

if __name__ == "__main__":
    import time
    
    identification = driverIdentificationDoorman('/dev/ttyUSB0', 48)
    identification.setPlaceId(1)
    identification.clean()
    while True:
        id = identification.getIdentification()
        if id != None:
            placeId = identification.getPlaceId()
            print(str(id) + '  ' + str(placeId) + '  ' + identification.getIdentifikationType())
            identification.semaphore(True)
            time.sleep(2)
            identification.semaphore(False)
            identification.getIdentification();
        else:
            print(None)
        time.sleep(0.05)
