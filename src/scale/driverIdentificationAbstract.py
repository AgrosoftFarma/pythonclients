class driverIdentificationAbstract(object):
    '''
    Abstraktni trida identifikace.
    '''
    placeIds = [] #Pole id mist identifikace.
    def __init__(self):        
        pass
    
    def setPlaceId(self, placeIds):
        if isinstance(placeIds, int):
            placeIds = [placeIds]
        self.placeIds = placeIds
    
    def getIdentification(self):
        return None

    def getPlaceId(self):
        return self.placeIds[0]
    
    def getIdentifikationType(self):
        return None
    
    def clean(self):
        '''
        Vyprazdni buffer identifikace
        '''
        pass
    
    def semaphore(self, state):
        pass
    