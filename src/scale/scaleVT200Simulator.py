#!/usr/bin/env python3

#Simulator vahy VT200

from time import sleep
import serial
import random
import time

if __name__ == "__main__":
    i = 0
    serialPort = serial.Serial('/dev/ttyUSB0', 2400, timeout=0.2)
    while True:
        i += 1
        
        if (int(time.time()) % 20) < 10:
            weight = 0
        else:
            weight = 25.679 
        

        data = bytes([random.randrange(255)]) #b'\x00' #status byte
        
        data = data + b'+' + str.encode(("{0:.3f}".format(weight)).rjust(6, '0'))   #str.encode(str(round(weight,3)).rjust(6, '0'))
        
        data = data + b'\r' #End Line
        
        serialPort.write(data)
        
        print(str(i) + ': ', end='')
        print(data)
        
        sleep(0.107)
