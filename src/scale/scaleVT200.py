#!/usr/bin/env python3

from scaleAbstract import scaleAbstract
import serial

# Nastaveni vahy: parametr 2.t = 02         - Kontinualni odesilani.
#                 parametr 2.d = 08         - 8 bit bez parity
#
#Kratky navod
# Po zapnuti a nabehnuti na 3 s stisknout "T". Na displeji se zobrazi "Fn 00".
# Sipkama vybrat funkci "FN 49" (Nastaveni.) Enter.
# Vybrat "Set up". Enter.
# Vybtar "Setup2" pro prvni seriak a "SETUP3" pro druhy. Enter
# Nastavit parametr:
#       3.t  01
#       3.1   0
#       3.2   0
#       3.3   0
#       3.4   0
#       3.5   1
#       3.6   0
#       3.7   0
#       3.8   0
#       3.b  24     - Prenosova rychlost
#       3.d  08     - Datove bity
#
# Escap vylezt.
# Ulozeni: na 3 s stisknout "T", vybrat "Store" a enter

# Servis (v KH) vah sef Sedlacek 604 279 903, technik Bejsovec 604 279 903

class scaleVT200(scaleAbstract):
    '''
    Trida pro vahu s vyhodnocovaci jednotkou VT200 (VT400).
    '''
    
    portName = None
    
    serialPort = None
    
    nStable = 10
    
    def __init__(self, port, nStable=10):
        if float(serial.VERSION) < 3.4:
            raise Exception('Nespravna verze PySerial. Minimalni pozadovana 3.4, instalovana: ' + str(serial.VERSION))
        
        self.nStable = nStable
        self.portName = port
        self.openPort()
        
    def openPort(self):
        self.serialPort = serial.Serial(self.portName, 2400, timeout=0.2)

    def getWeight(self):
        self.serialPort.flushInput()
        self.serialPort.read(1000000) #Vyctu vse co je v buferru a zahodim to, skonci na timeout. flushInput nefunguje uplne optimalne.
        lastVal = None
        val = None
        n = 0
        while True:
            pokus = 10
            while True:
                rPokus = 12
                while True:
                    c = self.serialPort.read(1)
                    if c == b'\r':
                        #Mam konec paketu
                        break
                    rPokus = rPokus - 1
                    if rPokus <= 0:
                        raise Exception('Chyba vahy! Nenalezen konec paketu.')
                line = self.serialPort.read(8)
                if line != None and line != b'':
                    break;
                pokus = pokus - 1
                if pokus <= 0:
                    raise Exception('Chyba vahy!')
            try:
                line = line[1:8].decode("utf-8").strip()
                line = line.replace(" ", "")
                val = float(line)
            except:
                val = None
            if val != lastVal or lastVal is None or val is None:
                lastVal = val
                n = 0
            else:
                n = n + 1
                if n >= self.nStable:
                    break             
        return lastVal * 1000 #VT200 vraci vahu v t.

if __name__ == "__main__":
    import time
    import sys
    
    portName = '/dev/ttyUSB1'
    if len(sys.argv) >= 2:
        portName = sys.argv[1]

    scale = scaleVT200(portName)
    scale.nStable = 5
    while True:
        print(scale.getWeight())
        time.sleep(1)
