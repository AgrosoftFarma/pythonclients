#!/usr/bin/env python3

from scaleAbstract import scaleAbstract
import requests
import re
import time


class scaleI35(scaleAbstract):
    '''
    Propojeni s vahou PRECIA MOLEN i35 pres ethernet.
    '''

    debug = False

    def __init__(self, url, timeout=5, maxTest=5, nStable=5):
        self.url = url
        self.timeout = timeout
        self.maxTest = maxTest
        self.nStable = nStable

    def getWeight(self):
        errorCouter = 0
        val = None
        valCouter = 0
        while True:
            try:
                a = requests.get(self.url + '/ValPoids.cgx', timeout=self.timeout)
                if a.status_code != 200:
                    raise Exception()
                m = re.search(r"<id>ValPoids<\/id><value>\s*(\d*[\.,\,]?\d*) kg<\/value>", a.text, re.MULTILINE)
                curVal = m.group(1).replace(",", ".").replace(" ", "")
                if curVal == val and curVal is not None:
                    valCouter += 1
                    if valCouter >= self.nStable:
                        return float(curVal)
                else:
                    val = curVal
                    valCouter = 0
                    errorCouter = 0
            except Exception as e:
                errorCouter += 1
                if self.debug:
                    print(e.args,  "errorCouter: " + str(errorCouter) + ", valCouter:" + str(valCouter) + ", val: " + str(val))
                if errorCouter >= self.maxTest:
                    raise e
                time.sleep(0.3)


if __name__ == "__main__":

    scale = scaleI35('http://192.168.20.5')
    scale.debug = True
    while True:
        print(scale.getWeight(), 'kg')
        time.sleep(1)
