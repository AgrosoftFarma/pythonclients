#!/usr/bin/env python3

from scaleAbstract import scaleAbstract
import random

class scaleSimulator(scaleAbstract):
    '''
    Simulace realne vahy.
    '''
    def __init__(self):
        pass

    def getWeight(self):
        return random.randrange(8000, 25000, 20)


if __name__ == "__main__":
    import time
    
    scale = scaleSimulator()
    while True:
        print(scale.getWeight())
        time.sleep(1)
