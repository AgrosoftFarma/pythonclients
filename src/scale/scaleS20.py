#!/usr/bin/env python3

from scaleAbstract import scaleAbstract
import serial

class scaleS20(scaleAbstract):
    '''
    Trida pro vahu s vyhodnocovaci jednotkou S20.
    '''
    
    portName = None
    
    serialPort = None
    
    nStable = 10
    
    def __init__(self, port, nStable=10):
        self.nStable = nStable
        self.portName = port
        self.openPort()
        
    def openPort(self):
        self.serialPort = serial.Serial(self.portName, 2400, timeout=0.5)

    def getWeight(self):
        self.serialPort.flushInput()
        lastVal = None
        val = None
        n = 0
        while True:
            pokus = 10
            while True:
                line = self.serialPort.readline()
                if line != None and line != b'':
                    break;
                pokus = pokus - 1
                if pokus <= 0:
                    raise Exception('Chyba vahy!')
            try:
                val = float(line[1:13].decode("utf-8").strip())                      
            except:
                val = None
            if val != lastVal or lastVal is None or val is None:
                lastVal = val
                n = 0
            else:
                n = n + 1
                if n >= self.nStable:
                    break             
        return lastVal

if __name__ == "__main__":
    import time
    
    scale = scaleS20('/dev/ttyUSB0')
    while True:
        print(scale.getWeight())
        time.sleep(1)
