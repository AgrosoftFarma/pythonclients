'''
Created on 24. 6. 2017

@author: krupkaf
'''

from . import common 

class RD(common.Common):

    def fetch(self, reportName, template, where=None):
        ret = self.query('rd.fetch', {
            'reportName': reportName,
            'template': template,
            'where': where,
            'errorFormat': 'json'
            }
        )
        return ret

    def download(self, reportName, template, where=None):
        ret = self.query('rd.fetch', {
            'reportName': reportName,
            'template': template,
            'where': where,
            'errorFormat': 'json'
            },
            binaryResponse = True)
        return ret
