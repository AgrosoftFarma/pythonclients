'''
'''

from . import common

class Doorman(common.Common):
    data = []
    def __init__(self):
        self.data = []

    def send(self):
        ret = self.query('doorman.timesheetsEventsAdd', {'timesheetsEvents': self.data})
        self.data = []
        return ret

    def addData(self, placeId, direction, transponderId, time):
        self.data.append({
            'placeId': placeId,
            'direction': direction,
            'transponderId': transponderId,
            'time': time
            })
