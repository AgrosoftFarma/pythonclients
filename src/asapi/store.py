'''
Created on 19. 6. 2017

@author: krupkaf
'''

from . import common 

class Store(common.Common):
    data = []
    def __init__(self):
        self.data = []

    def send(self):
        ret = self.query('store.wpEventsAdd', {'wpEvents': self.data})
        self.data = []        
        return ret
    
    def addData(self, time, driverTransponderId, placeId, weight, vehicleTransponderIds, note):
        self.data.append({
            'time': time,
            'driverTransponderId': driverTransponderId,
            'placeId': placeId,
            'weight': weight,
            'vehicleTransponderIds': vehicleTransponderIds,
            'note': note
            })
