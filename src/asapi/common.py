'''
Created on 16. 6. 2017

@author: krupkaf
'''

import queue    #Pouze pro donuceni pyinstaller.exe, aby modul pribalil. Jinak tady neni potreba.
import requests
import json

class ASapiException(Exception):
        def __init__(self, code=None, msg=None):
            self.code = code
            self.msg = msg
            pass;


class Common(object):
    serverUrl = None
    userUid = None
    userName = None
    userPassword = None
    appName = None
    companyId = None
    verify_ssl = False

    def __init__(self):
        pass

    def setLogin(self, url, userName, userPassword, appName, companyId):
        Common.userUid = None
        Common.serverUrl = url
        Common.userName = userName
        Common.userPassword = userPassword
        Common.appName = appName
        Common.companyId = companyId

    def setCompanyId(self, companyId):
        Common.companyId = companyId

    def query(self, method, params=None, binaryResponse=False):
        '''
        Odesle dotaz na server a prijme a provede zakladni zpracovani odpovedi.
        '''
        if self.isLogged() == False:
            if (method not in ['user.isLogged', 'user.login']):
                self.login()

        if params == None:
            params = {}
        if 'userUid' not in params:
            params['userUid'] = Common.userUid
        if 'companyId' not in params:
            params['companyId'] = Common.companyId
        payload = {
            "method": method,
            "params": params,
            "jsonrpc": "2.0"
        }
        if binaryResponse == None:
            binaryResponse = False
        headers = {'content-type': 'application/json',
                   'X-Requested-With': 'XMLHttpRequest'}
        bResponse = requests.post(Common.serverUrl, json.dumps(payload), headers=headers, verify=self.verify_ssl)
        if binaryResponse != True:
            response = json.loads(bResponse.content.decode("utf-8"))
        else:
            #Ocekavam binarni data, NE json
            try:
                #Zkusim, zda to precejenom neni json
                response = bResponse.json()
                #Pokud to je json, zrejme je to chyba a dale to zpracuji jako json.
            except Exception as exception:
                #Takova berlicka. Na WinXP neexistuje json.decoder.JSONDecodeError. Jinde je zase potraba pouzivat json.decoder.JSONDecodeError.
                if type(exception).__name__ == 'ValueError' or type(exception).__name__ == 'JSONDecodeError':
                    #Odpoved nelze interpretovat jako json, takze to jsou obecna binarni data, vratim je.
                    return bResponse.content

        if 'error' in response:
            code = None
            msg = None
            if 'code' in response['error']:
                code = response['error']['code']
            if 'shortMsg' in response['error']:
                msg = response['error']['shortMsg']
            if msg == 'Neplatné přihlášení!':
                Common.userUid = None  # userUid je zrejme neplatne
                try:
                    del params['userUid']
                except KeyError:
                    pass
                return self.query(method, params)  # A zkusim to znovu.
            raise ASapiException(code, msg)

        if not('result' in response):
            raise ASapiException(0, 'V odpovědi není položka "result"!')

        return response['result']

    def isLogged(self, fast=True):
        '''
        Pokud je fast == True provadi se pouze test nastaveni userUid/
        Pokud je fast == False a je nastaven userUid, teprve pak se dela dotaz na server.
        '''
        if Common.userUid == None:
            return False
        if fast == True:
            return True
        res = self.query('user.isLogged', {"userUid":None, "testUserUid":Common.userUid});
        if res != 'Not logged.':
            return True
        return False

    def login(self):
        '''
        Provede prihlaseni na server
        '''
        if Common.serverUrl == None \
            or Common.userName == None \
            or Common.userPassword == None:
            raise ASapiException(0, 'Nejsou nastaveny přihlašovací údaje!')
        params = {
            'userName': Common.userName,
            'password': Common.userPassword,
            'url': Common.appName
        }
        response = self.query('user.login', params)
        Common.userUid = response['userUid']

    def logout(self):
        '''
        Provede odhlaseni od serveru
        '''
        self.query('user.logout', {})
        Common.userUid = None
