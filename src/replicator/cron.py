'''
Created on 9. 3. 2018
@author: krupkaf
'''

import time

class Cron(object):
    '''
    Trida pro zpracovani fronty uloh pro relikator
    '''

    avableTasks = ['helloWorld', 'transpondersUpgrade', 'scaleData', 'doormanData']

    fronta = None

    errorSleep = 60  #Cas, ktery se ceka, pokud nastala ve vykonavani akce chyba, nez se zkusi znovu.

    def __init__(self):
        self.fronta = {}

    def taskAdd(self, taskName, period):
        '''
        Prida novou akci do fronty.
        '''
        if taskName in self.avableTasks:
            #Pokud je uloha mezi podporovanymi, pridam ji. Pokud jiz byla pridana, prepise se.
            self.fronta[taskName] = {
                'period': period,
                'nextRun' : 0
            }
        else:
            #Pokud uloha neni mezi podporovanymi, prehlizim to a nedelam nic.
            pass

    def taskDel(self, taskName):
        '''
        Trvale odebere ulohu z fronty
        '''
        del self.fronta[taskName]

    def getTasks(self):
        '''
        Vrati pole jmen akci, ktere je potreba provest.
        '''
        now = int(time.time());
        ret = []
        for taskName in self.fronta:
            task = self.fronta[taskName]
            if task['nextRun'] <= now:
                ret.append(taskName)
        return ret

    def taskOk(self, taskName):
        '''
        Informuje frontu uloh, ze uloha byla provedena bez problemu.
        '''
        self.taskNextRun(taskName)

    def taskError(self, taskName):
        '''
        Informuje front, ze uloha zhavarovala.
        '''
        self.taskNextRun(taskName, self.errorSleep)

    def taskNextRun(self, taskName, addTime = None):
        if taskName in self.fronta:
            #Overim si, ze uloha existuje. Pokud neexistuje, neresim to, nevsimam si toho.
            task = self.fronta[taskName];
            if addTime == None:
                addTime = task['period']

            self.fronta[taskName] = {
                'period': task['period'],
                'nextRun' : int(time.time()) + addTime
            }
