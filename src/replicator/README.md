# Replicator

Samostatny replicator pro vymenu dat mezi serverem a lokalni databazi v SQLite.

Pouze replicator udrzuje strukturu databaze a vytvari novou, pokud neexistuje.

## Instalace

* Pro replicator je vhodne pouzit samostatneho uzivatele - agrosoft.
* Adresar src/replicator se zkopiruje do /opt/agrosoft/replicator
* Replicator zavisi na modulu [ASApi](https://gitlab.com/Skotsoft/PythonClients/tree/master/src/asapi). Adresar src/asapi se zkopiruje do /opt/agrosoft/asapi
* Veskere soubory *.py a prislusne adresara by mel vlastnit uzivatel agrosoft.
* Priklad konfigurace replicatoru [replicator.conf.example](https://gitlab.com/Skotsoft/PythonClients/blob/master/src/config/replicator.conf.example)
    * Konfigurace se ulozi do souboru /opt/agrosoft/config/replicator.conf
    * Opravneni na konfiguraci by melo byt 0600 - obsahuje prihlasovaci udaje na server.
    * Je nutne pripravit adresar pro ulozeni DB s vhodnymi opravnenimi. Databazi si vytvori replicator.
* Spusteni replicatoru jako sluzbu pod systemd
    * Zkopirovat (dle potrebi upravit) soubor [systemd/areplicator.service](https://gitlab.com/Skotsoft/PythonClients/blob/master/systemd/areplicator.service) do /etc/systemd/system/. Upravit opravneni na 0755 a vlastnika na root:root
    * Povoleni sluzby: `systemctl enable areplicator.service`
    * Rucni spusteni: `systemctl start areplicator.service`

## Log
Replicator vypisuje log na konzoli. Pokud je replicator spusteny jako sluzba systemd, systemd logy odchytava a standardne zpracovava.
* `systemctl status areplicator.service`
* `journalctl --unit=areplicator.service`
