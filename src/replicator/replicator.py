#!/usr/bin/env python3
'''
Created on 8. 3. 2018
@author: krupkaf
'''

import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../')

import configparser
import sqlite3
import time
import cron
import asapi.rd
import asapi.store
import asapi.doorman
import datetime
import logging
import signal

loopStop = False

def signal_term_handler(signal, frame):
    global loopStop
    loopStop = True

signal.signal(signal.SIGTERM, signal_term_handler)
signal.signal(signal.SIGINT, signal_term_handler)

config = configparser.ConfigParser()
config.read(os.path.dirname(os.path.abspath(__file__)) + '/../config/replicator.conf')
logLevel = config.getint('replicator', 'logLevel', fallback=40)

logger = logging.getLogger()
logger.setLevel(logLevel)
h = logging.StreamHandler()
h.setLevel(logLevel)
h.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s'))
logger.addHandler(h)

logger.debug('Start')

sqlS = sqlite3.connect(config.get('db', 'fileName'))
for sql in open(os.path.dirname(os.path.abspath(__file__)) + '/createDB.sql', 'r').read().split(';'):
    sqlS.execute(sql)
    sqlS.commit()

cron = cron.Cron()

#Nactu z configu co a jak casto se bude replikovat.
for item in cron.avableTasks:
    period = config.getint('replicating', item, fallback=-1)
    if period > 0:
        cron.taskAdd(item, period)

cron.errorSleep = config.getint('replicator', 'errorSleep', fallback=60) #Pokud operace selze, cas v s za jaky se to zkusi znovu.

companyIds = [ int(x) for x in config.get('ASApiServer', 'companyIds').split(' ') ]

rd = asapi.rd.RD()
rd.setLogin(
            config.get('ASApiServer', 'url'),
            config.get('ASApiServer', 'userName'),
            config.get('ASApiServer', 'userPassword'),
            'skladReplicator',
            companyIds[0])
logger.debug('Login')
rd.login()

#*****************************************************************************************************
def transpondersUpgrade():
    for companyIndex in range(len(companyIds)):
        rd.setCompanyId(companyIds[companyIndex])
        transponders = rd.fetch('doorman.transponders', 'doorman.transponders', [{"column":"enable", "condition":"=", "value":True}])
        transponderIdSs = []
        for transponder in transponders['items']:
            transponderIdSs.append(transponder['id'])
            a = sqlS.execute("SELECT id "
                    + "FROM transponders "
                    + "WHERE id_s = " + str(transponder['id']) + " AND company_id = " + str(companyIds[companyIndex])).fetchone()
            if a != None:
                a = a[0]
            if a is None:
                c = sqlS.cursor()
                c.execute("INSERT INTO transponders(id_s, company_id, priority, first_name, last_name, number, transponder, hcs301, enable) VALUES "
                        + "(" + str(transponder['id']) + ","
                        + str(companyIds[companyIndex]) + ","
                        + str(companyIndex) + ","
                        + "'" + str(transponder['firstName']) + "',"
                        + "'" + str(transponder['lastName']) + "',"
                        + "'" + str(transponder['number']) + "',"
                        + (("'" + str(transponder['transponder']).strip().upper() + "'") if transponder['transponder'] != None else 'NULL') + ","
                        + (("'" + str(transponder['hcs301']).strip().upper() + "'") if transponder['hcs301'] != None else 'NULL') + ","
                        + ('1' if transponder['enable'] == True else '0') + ")")
                sqlS.commit()
            else:
                c = sqlS.cursor()
                c.execute("UPDATE transponders SET "
                        + "id_s = " + str(transponder['id']) + ", "
                        + "company_id = " + str(companyIds[companyIndex]) + ", "
                        + "priority = " + str(companyIndex) + ", "
                        + "first_name = '" + str(transponder['firstName']) + "', "
                        + "last_name = '" + str(transponder['lastName']) + "', "
                        + "number = '" + str(transponder['number']) + "', "
                        + "transponder = " + (("'" + str(transponder['transponder']).strip().upper() + "'") if transponder['transponder'] != None else 'NULL') + ","
                        + "hcs301 = " + (("'" + str(transponder['hcs301']).strip().upper() + "'") if transponder['hcs301'] != None else 'NULL') + ","
                        + "enable = " + ('1' if transponder['enable'] == True else '0')
                        + " WHERE id = " + str(a) + ";")
                sqlS.commit()
        #Smazani transponderu, ktere na serveru jiz neexistuji.
        c = sqlS.cursor()
        c.execute("DELETE FROM transponders "
                + "WHERE id_s NOT IN (" + (', '.join(str(x) for x in transponderIdSs)) + ") "
                + "AND company_id = " + str(companyIds[companyIndex]))
        sqlS.commit()
#*****************************************************************************************************
def scaleData():
    for companyIndex in range(len(companyIds)):
        sql = "SELECT wevents.id, wevents.time, transponders.id_s, place_id, brutto, vehicle_transponder_ids, note "
        sql += "FROM wevents "
        sql += "LEFT JOIN transponders ON transponders.id = wevents.driver_transponder_id "
        sql += "WHERE send_time IS NULL "
        sql += "      AND (transponders.company_id = " + str(companyIds[companyIndex])
        if companyIds[0] == companyIds[companyIndex]:
            sql += " OR wevents.driver_transponder_id IS NULL "
        sql += "          )"
        sql += " LIMIT 20"

        a = sqlS.execute(sql).fetchall()
        if len(a) == 0:
            logger.info('No data, companyId = ' + str(companyIds[companyIndex]))
            continue
        aIds = [] #Id odeslanych zaznamu v lokalni db tabulka wevents.
        store = asapi.store.Store()
        store.setCompanyId(companyIds[companyIndex])
        for line in a:
            aIds.append(line[0])
            vIdSs = []
            if (line[5] != None):
                vIds = line[5].strip('{}').split(',')
                for vId in vIds:
                    a = sqlS.execute('SELECT id_s FROM transponders WHERE id = ' + vId + ' ORDER BY priority LIMIT 1').fetchone();
                    if a != None:
                        vIdSs.append(a[0])
            store.addData(datetime.datetime.fromtimestamp(line[1]).strftime('%Y-%m-%d %H:%M:%S'),
                          line[2],
                          line[3],
                          line[4],
                          vIdSs,
                          line[6])
        logger.info('Send, companyId = ' + str(companyIds[companyIndex]))
        store.send()
        c = sqlS.cursor()
        c.execute("UPDATE wevents SET send_time = strftime('%s', 'now') WHERE id IN (" + (', '.join(str(x) for x in aIds)) + ")")
        sqlS.commit()
#*****************************************************************************************************
def doormanData():
    for companyIndex in range(len(companyIds)):
        a = sqlS.execute("SELECT devents.id, devents.place_id, devents.direction, transponders.id_s, devents.time "
                         + " FROM devents "
                         + " LEFT JOIN transponders ON transponders.id = devents.transponder_id "
                         + " WHERE devents.send_time IS NULL "
	                     + "       AND transponders.company_id = " + str(companyIds[companyIndex])
                         + " LIMIT 20").fetchall()
        if len(a) == 0:
            logger.info('No data, companyId = ' + str(companyIds[companyIndex]))
            continue
        aIds = [] #Id odeslanych zaznamu
        d = asapi.doorman.Doorman()
        d.setCompanyId(companyIds[companyIndex])
        for line in a:
            aIds.append(line[0])
            d.addData(line[1],
                      line[2],
                      line[3],
                      datetime.datetime.fromtimestamp(line[4]).strftime('%Y-%m-%d %H:%M:%S'))
        logger.info('Send, companyId = ' + str(companyIds[companyIndex]))
        d.send()
        c = sqlS.cursor()
        c.execute("UPDATE devents SET send_time = strftime('%s', 'now') WHERE id IN (" + (', '.join(str(x) for x in aIds)) + ")")
        sqlS.commit()
#*****************************************************************************************************
i = 0
while loopStop == False:
    i = i + 1
    logger.debug("i: " + str(i))
    time.sleep(1)
    for action in cron.getTasks():
        try:
            logger.info(time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()) + '  ' + action + ':... ')
            if action == 'scaleData':
                scaleData()
            elif action == 'transpondersUpgrade':
                transpondersUpgrade()
            elif action == 'helloWorld':
                print("Hello World!")
            elif action == 'doormanData':
                doormanData()
            else:
                cron.taskDel(action) #Zakazu provadeni akce, je nepodporovana.
            cron.taskOk(action)
            logger.info('Ok')
        except Exception:
            logger.error(sys.exc_info())
            cron.taskError(action)

sqlS.close()
rd.logout()
logger.debug('End')
