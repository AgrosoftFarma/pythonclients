CREATE TABLE IF NOT EXISTS transponders
(
  id integer PRIMARY KEY autoincrement,
  id_s int NOT NULL, --ID transponderu na serveru
  company_id int NOT NULL, --ID firmy na serveru
  priority int NOT NULL, --Priorita vyberu transponderu, pokud maji stejnou identifikaci. Mensi se bere drive.
  first_name text,
  last_name text,
  "number" text,
  transponder text,
  hcs301 text,
  enable boolean NOT NULL DEFAULT false
);

CREATE TABLE IF NOT EXISTS wevents
(
  id INTEGER PRIMARY KEY,
  time int NOT NULL,
  place_id int NOT NULL,
  driver_transponder_id int,
  vehicle_transponder_ids int[],
  note text NOT NULL default '',
  brutto real NOT NULL,
  send_time int DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS devents
(
  id INTEGER PRIMARY KEY,
  place_id int NOT NULL,
  direction char NOT NULL,
  transponder_id int NOT NULL,
  time int NOT NULL,
  send_time int DEFAULT NULL
);
