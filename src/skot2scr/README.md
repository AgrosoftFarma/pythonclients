# Generování spojovacího souboru ze SKOTu do SCR Heatime.

## Spuštěni
    $ source ./activate.bash
    $ ./skot2scr.py


## Vytvoření binárky
    $ source ./activate.bash
    $ pyinstaller --onefile skot2scr.py


## Vytvoření binárky exe pro Win na Linuxu
Instalovat vždy aktuální verze. Pod Wine Python 3.6 funguje, novější verze zatím ne.

    $ cd ~/.wine/drive_c/
    $ wget https://www.python.org/ftp/python/3.6.4/python-3.6.4.exe
    $ ln -s ~/skotsoft.PythonClients/ skotsoft.PythonClients  #Projekt je v C:\skotsoft.PythonClients
    $ wineconsole

Ve wineconsole

    python-3.6.4.exe
    python.exe
    cd C:\skotsoft.PythonClients\src\skot2scr
    set PYTHONPATH=%PYTHONPATH%;C:\skotsoft.PythonClients\src\
    pip.exe install pyinstaller requests

    python.exe skot2scr.py   #test funkce
    pyinstaller --onefile skot2scr.py

V dist je exe.

## Instalace na Win (10)

* soubory skot2scr.conf (pro daného uživatele) a skot2scr.exe nahrát do adresáře C:\Agrosoft\skot
* otevřít Plánovač úloh
* varianta 1:
    * kliknout na importovat úlohu a načíst soubor s konfigurací úlohy: [synchronizace_SKOT_SCR.xml](./doc/synchronizace_SKOT_SCR.xml)
* varianta 2:
    * kliknout na Vytvořit úlohu a nakonfigurovat podle obrázků v pořadí 1 až 5
    * ![1](./doc/1.png)
    * ![2](./doc/2.png)
    * ![3](./doc/3.png)
    * ![4](./doc/4.png)
    * ![5](./doc/5.png)
* Potvrdit vytvoření úlohy tlačítkem ok
* Zrestartovat počítač.
 

