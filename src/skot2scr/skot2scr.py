#!/usr/bin/env python3

import os
import sys
import asapi.rd
import configparser
import time


def sleep():
    print()
    for i in range(0, 3):
        print('.', end='', flush=True)
        time.sleep(1)
    print()


config_file_name = os.getcwd() + os.sep + 'skot2scr.conf'
if os.path.isfile(config_file_name) is not True:
    print("Soubor s konfiguraci nenalezen!")
    print(config_file_name)
    sleep()
    sys.exit(-1)


config = configparser.ConfigParser()
config.read(config_file_name)
print('Start')
rd = asapi.rd.RD()
rd.setLogin(
    config.get('ASApiServer', 'url'),
    config.get('ASApiServer', 'userName'),
    config.get('ASApiServer', 'userPassword'),
    'skot2scr',
    config.get('ASApiServer', 'companyId'))
print('Přihlášení')
rd.login()
print('Stažení dat')
data = rd.download('skot.techSCR', 'skot.techSCR')
print('Odhlášení')
rd.logout()

print('Uložení dat')
with open(config.get('skot2scr', 'fileName'), 'wb') as file:
    file.write(data)

print('Hotovo')
sleep()
