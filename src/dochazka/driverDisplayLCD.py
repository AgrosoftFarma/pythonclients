import libmodbusdevicesPy.displayLCD

class driverDisplayLCD():
    '''
    '''
    display = None

    def __init__(self, port, mbAddress):
        self.display = libmodbusdevicesPy.displayLCD.DisplayLCD(port, mbAddress)

    def flush(self, line0, line1):
        self.display.write(line0, line1)

if __name__ == "__main__":
    import time
    d = driverDisplayLCD('/dev/ttyUSB0', 34)
    d.flush('123456789012345678', '    ddd')
