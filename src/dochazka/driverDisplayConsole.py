#!/usr/bin/env python3

class driverDisplayConsole():
    '''
    Trida simulace displeje.
    '''
    def __init__(self):
        pass

    def flush(self, line0, line1):
        print("\033[%d;%dH" % (2, 0))
        print(chr(0x2554) + chr(0x2550) + chr(0x2550) + chr(0x2550) + chr(0x2550)
             + chr(0x2550) + chr(0x2550) + chr(0x2550) + chr(0x2550)
             + chr(0x2550) + chr(0x2550) + chr(0x2550) + chr(0x2550)
             + chr(0x2550) + chr(0x2550) + chr(0x2550) + chr(0x2550) + chr(0x2557))
        print(chr(0x2551) + line0.ljust(16)[:16] + chr(0x2551))
        print(chr(0x2551) + line1.ljust(16)[:16] + chr(0x2551))
        print(chr(0x255A) + chr(0x2550) + chr(0x2550) + chr(0x2550) + chr(0x2550)
             + chr(0x2550) + chr(0x2550) + chr(0x2550) + chr(0x2550)
             + chr(0x2550) + chr(0x2550) + chr(0x2550) + chr(0x2550)
             + chr(0x2550) + chr(0x2550) + chr(0x2550) + chr(0x2550) + chr(0x255D))

if __name__ == "__main__":
    import time
    d = driverDisplayConsole()
    d.flush('123456789012345678', '    ddd')
