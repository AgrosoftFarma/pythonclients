#!/usr/bin/env python3
'''
'''
import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../')
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../scale/')

import logging
import configparser
from time import sleep
import signal
import sqlite3
import traceback
from datetime import datetime

loopStop = False

def signal_term_handler(signal, frame):
    global loopStop
    loopStop = True

def save(transponderNumber, direction, placeId, time):
    transponderNumber = str(transponderNumber).strip()
    transponderId = None
    transponderName = None
    a = sqlS.execute("SELECT id, last_name || ' ' || first_name AS name FROM transponders WHERE transponder = upper('" + transponderNumber + "') ORDER BY priority asc, id desc LIMIT 1").fetchone()
    if a != None:
        transponderId, transponderName = a
    else:
        transponderName = 'Neznamy cip!'

    if transponderId != None:
        logger.info(transponderName + '; ' + direction + '; ' + str(placeId))
        sql = ("INSERT INTO devents(time, place_id, transponder_id, direction) VALUES "
            + "(" + time.strftime('%s') + ", "
            + str(placeId) + ", "
            + str(transponderId) + ", "
            + "'" + str(direction) + "')")
        logger.debug(sql)
        c = sqlS.cursor()
        c.execute(sql)
        sqlS.commit()

    return transponderName


if __name__ == "__main__":
    signal.signal(signal.SIGTERM, signal_term_handler)
    signal.signal(signal.SIGINT, signal_term_handler)

    config = configparser.ConfigParser()
    config.read(os.path.dirname(os.path.abspath(__file__)) + '/../config/dochazka.conf')
    logLevel = config.getint('dochazka', 'logLevel', fallback=40)

    logger = logging.getLogger()
    logger.setLevel(logLevel)
    h = logging.StreamHandler()
    h.setLevel(logLevel)
    h.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s'))
    logger.addHandler(h)

    #Pripojeni k DB
    sqlS = sqlite3.connect(config.get('db', 'fileName'))

    sys.path.append("../scale")

    #Inicializace identifikace
    identification = []
    i = 1
    while config.has_option('identification' + str(i), 'type'):
        n = None;
        dIdentType = config.get('identification' + str(i), 'type')
        if dIdentType == 'simulator':
            from driverIdentificationSimulator import driverIdentificationSimulator
            n = driverIdentificationSimulator(sqlS);
        elif dIdentType == 'doorman':
            from driverIdentificationDoorman import driverIdentificationDoorman
            n = driverIdentificationDoorman(config.get('dochazka', 'port'), config.getint('identification' + str(i), 'modBusAddress'));
        else:
            logger.error('Chyba konfigurace. Nepodporovany typ identifikace. ' + dIdentType)
            sys.exit()
        n.direction = config.get('identification' + str(i), 'direction')
        identification.append(n)
        i += 1

    if identification == []:
        logger.error('Chyba konfigurace. Není zadán identifikace.')
        sys.exit()

    #Inicializace displeje
    display = None
    displayType = config.get('display', 'type')
    if displayType == 'console':
        from driverDisplayConsole import driverDisplayConsole
        display = driverDisplayConsole();
    elif displayType == 'LCD':
        from driverDisplayLCD import driverDisplayLCD
        display = driverDisplayLCD(config.get('dochazka', 'port'), config.getint('display', 'modBusAddress'));
    else:
        logger.error('Chyba konfigurace. Nepodporovany typ displeje. ' + displayType)
        sys.exit()

    placeId = config.getint('dochazka', 'placeId')
    if placeId == None:
        logger.error('Chyba konfigurace. Není zadán id mista.')
        sys.exit()

    displayTime = config.getint('dochazka', 'displayTime', fallback=10)

    logger.debug('Start')

    displayL0 = ''
    displayL1 = ''
    eventTime = None
    transponderNumberBefore = None
    try:
        #Prectu identifikaci ze vsech zdroju, abych vyloucil, ze se vezme neco stareho.
        for ident in identification:
            ident.clean()
        while loopStop == False:
            sleep(0.2)
            now = datetime.now()

            for ident in identification:
                transponderNumber = ident.getIdentification()
                if transponderNumber != None:
                    ident.clean()
                if (transponderNumber != None) and (transponderNumber != transponderNumberBefore):
                    eventTime = now
                    direction = ident.direction
                    transponderName = save(transponderNumber, ident.direction, placeId, eventTime)
                    for ident in identification:
                        ident.clean()
                    if transponderName == 'Neznamy cip!':
                        displayL0 = 'Chyba'.ljust(16)[:16]
                        displayL1 = transponderName.ljust(16)[:16]
                        logger.warning('Neznamy cip ' + transponderNumber)
                    else:
                        smer = ''
                        if direction == 'i':
                            smer = 'Prichod'
                        elif direction == 'o':
                            smer = 'Odchod'
                        else:
                            smer = '?'

                        displayL0 = smer.ljust(8) + eventTime.strftime('%T')
                        displayL1 = transponderName
                    transponderNumberBefore = transponderNumber
                    break

            if eventTime == None or (now - eventTime).total_seconds() > displayTime:
                transponderNumberBefore = None
                displayL0 = now.strftime("  %d.%m. %Y")
                displayL1 = now.strftime("    %T")

            if display != None:
                display.flush(displayL0, displayL1)

    except BaseException as e:
        logger.error(traceback.format_exc())

    sqlS.close()
    logger.debug('End')
