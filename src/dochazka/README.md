# Obsluha vahy

Obsluha dochazky. Data uklada do lokalni sqlite databaze odkud si je bere [Replicator](https://gitlab.com/AgrosoftFarma/PythonClients/tree/master/src/replicator) a predava na server.
Dochazka si sam neumi zalozit DB. DB musi predem vytvorit replicator.

## Instalace

* Pro Dochazku je vhodne pouzit samostatneho uzivatele - agrosoft.
* Adresar src/dochazka se zkopiruje do /opt/agrosoft/dochazka
* Veskere soubory *.py a prislusne adresar by mel vlastnit uzivatel agrosoft.
* Zavislosti
    * [libmodbusdevicesPy](https://gitlab.com/AgrosoftModBus/libmodbusdevicesPy)
        * Zkopirovat adresar libmodbusdevicesPy projektu libmodbusdevicesPy do /opt/agrosoft/libmodbusdevicesPy
    * driverIdentificationDoorman
        * Adresar src/scale se zkopiruje do /opt/agrosoft/scale
* Priklad konfigurace [scale.conf.example](https://gitlab.com/AgrosoftFarma/PythonClients/blob/master/src/config/dochazka.conf.example)
    * Konfigurace se ulozi do souboru /opt/agrosoft/config/dochazka.conf
* Spusteni Dochazky jako sluzbu pod systemd
    * Zkopirovat (dle potrebi upravit) soubor [systemd/adochazka.service](https://gitlab.com/AgrosoftFarma/PythonClients/blob/master/systemd/adochazka.service) do /etc/systemd/system/. Upravit opravneni na 0755 a vlastnika na root:root
    * Povoleni sluzby: `systemctl enable adochazka.service`
    * Rucni spusteni: `systemctl start adochazka.service`

## Log
Dochazka vypisuje log na konzoli. Pokud je Dochazka spustena jako sluzba systemd, systemd logy odchytava a standardne zpracovava.
* `systemctl status adochazka.service`
* `journalctl --unit=adochazka.service`
