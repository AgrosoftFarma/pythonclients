#!/usr/bin/env python3
import configparser
import os
import asapi.rd

config = configparser.ConfigParser()
config.read(os.path.dirname(os.path.abspath(__file__)) + '/../../config/skot2fastos.conf')
print('Start')
rd = asapi.rd.RD()
rd.setLogin(
    config.get('ASApiServer', 'url'),
    config.get('ASApiServer', 'userName'),
    config.get('ASApiServer', 'userPassword'),
    'skot2fastos',
    config.get('ASApiServer', 'companyId'))
print('Přihlášení')
rd.login()
print('Stažení dat')
data = rd.download('skot.techFastos', 'skot.fastosDB')
print('Odhlášení')
rd.logout()

print('Uložení dat')
with open(config.get('skot2fastos', 'fileName'), 'wb') as file:
    file.write(data)
    
print('Hotovo')

input("Enter pro konec ...")