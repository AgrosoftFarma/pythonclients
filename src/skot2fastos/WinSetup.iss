
[Setup]
AppName="skot2fastos"
AppVersion="20170917"
AppPublisher="Agrosoft T�bor s.r.o."
AppPublisherURL="agrosoft.cz"
AppSupportURL=
AppUpdatesURL=
DefaultDirName=C:\Agrosoft\skot2fastos
SetupIconFile=C:\Agrosoft.build\skot2fastos\favicon.ico
Compression=lzma
SolidCompression=yes
ChangesAssociations=yes
OutputDir=Z:\home\krupkaf\Data\aprac\skotsoft.PythonClients\src\skot2fastos\
OutputBaseFilename=SetupSkot2fastos
DefaultGroupName="Agrosoft"

[Files]
Source: "C:\Agrosoft.build\skot2fastos\*"; DestDir: "C:\Agrosoft\skot2fastos"; Flags: ignoreversion recursesubdirs

[Dirs]
Name: "C:\Agrosoft\agrdata"

[Icons]
Name: "{commondesktop}\Skot2Fastos"; Filename: "C:\Agrosoft\skot2fastos\skot2fastos.exe"; WorkingDir: "C:\Agrosoft\skot2fastos\"; IconFilename: C:\Agrosoft\skot2fastos\favicon.ico
Name: "{group}\Skot2Fastos"; Filename: "C:\Agrosoft\skot2fastos\skot2fastos.exe"; IconFilename: C:\Agrosoft\skot2fastos\favicon.ico
