#!/usr/bin/env python3
'''
Created on 13. 7. 2017

@author: krupkaf
'''
import serial

ser = serial.Serial('/dev/ttyUSB0', 9600, timeout=0.5)
nStable = 20

def getWeight():
    ser.flushInput()
    lastVal = None
    val = None
    n = 0
    while True:
        line = ser.readline()
        val = float(line.decode("utf-8").strip())
        if val != lastVal or lastVal is None:
            lastVal = val
            n = n + 1
        else:
            n = n + 1
            if n >= nStable:
                break             
    return lastVal


i = 0
while True:
    i = i + 1
    print(str(i) + ': ' + str(getWeight()))
    
