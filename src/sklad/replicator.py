#!/usr/bin/env python3
'''
Created on 4. 7. 2017
@author: krupkaf
'''

import sqlite3
import sys
import configparser
import os
from time import sleep, gmtime, strftime
import datetime
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../')
import asapi.rd
import asapi.store

config = configparser.ConfigParser()
config.read(os.path.dirname(os.path.abspath(__file__)) + '/../../config/sklad.conf')

print('Start')

sqlS = sqlite3.connect(config.get('db', 'fileName'))
for sql in open(os.path.dirname(os.path.abspath(__file__)) + '/createDB.sql', 'r').read().split(';'):
    sqlS.execute(sql);
for sql in open(os.path.dirname(os.path.abspath(__file__)) + '/initData.sql', 'r').read().split(';'):
    sqlS.execute(sql);

rd = asapi.rd.RD()
rd.setLogin(
    config.get('ASApiServer', 'url'),
    config.get('ASApiServer', 'userName'),
    config.get('ASApiServer', 'userPassword'),
    'skladReplicator',
    config.get('ASApiServer', 'companyId'))
print('Login')
rd.login()

#*****************************************************************************************************
def transpondersUpgrade():
    transponders = rd.fetch('doorman.transponders', 'doorman.transponders', [{"column":"enable","condition":"=","value":True}])
    transponderIds = []
    for transponder in transponders['items']:
        transponderIds.append(transponder['id'])
        a = sqlS.execute("SELECT id FROM transponders WHERE id = " + str(transponder['id'])).fetchone()
        if a is None:
            c = sqlS.cursor()
            c.execute("INSERT INTO transponders(id, first_name, last_name, number, transponder, enable) VALUES "
                      + "(" + str(transponder['id']) + ","
                      + "'" + str(transponder['firstName']) + "',"
                      + "'" + str(transponder['lastName']) + "',"
                      + "'" + str(transponder['number']) + "',"
                      + (("'" + str(transponder['transponder']).strip() + "'") if transponder['transponder'] != None else 'NULL') + ","
                      + ('1' if transponder['enable'] == True else '0') + ")")
            sqlS.commit()
        else:
            c = sqlS.cursor()
            c.execute("UPDATE transponders SET "
                      + "first_name = '" + str(transponder['firstName']) + "',"
                      + "last_name = '" + str(transponder['lastName']) + "',"
                      + "number = '" + str(transponder['number']) + "',"
                      + "transponder = " + (("'" + (transponder['transponder']).strip() + "'") if transponder['transponder'] != None else 'NULL') + ","
                      + "enable = " + ('1' if transponder['enable'] == True else '0')
                      + " WHERE id = " + str(transponder['id']) + ";")
            sqlS.commit()    
    c = sqlS.cursor()
    c.execute("DELETE FROM transponders WHERE id NOT IN (" + (', '.join(str(x) for x in transponderIds)) + ")")
    sqlS.commit()    
#*****************************************************************************************************
def sendData():
    a = sqlS.execute("SELECT id, time, driver_transponder_id, place_id, brutto, vehicle_transponder_ids, note  FROM wevents WHERE send_time IS NULL").fetchall()    
    if len(a) == 0:
        print('No data ', end='')
        return
    aIds = []
    store = asapi.store.Store()
    for line in a:
        aIds.append(line[0])
        store.addData(
            datetime.datetime.fromtimestamp(line[1]).strftime('%Y-%m-%d %H:%M:%S'),
            line[2],
            line[3],
            line[4],
            [] if line[5] is None else line[5],
            line[6])
    print('Send ', end='')
    store.send()
    c = sqlS.cursor()
    c.execute("UPDATE wevents SET send_time = strftime('%s', 'now') WHERE id IN (" + (', '.join(str(x) for x in aIds)) + ")")
    sqlS.commit()
#*****************************************************************************************************
try:
    i = 0;
    while True:
        i = i + 1
        print(str(i) + ":")
        sleep(0.5)
        actions = sqlS.execute("SELECT action_name FROM cron WHERE last_run + period <= CAST(strftime('%s', 'now') as decimal)")
        for action in actions:
            try:
                action = action[0]
                print(strftime("%Y-%m-%d %H:%M:%S", gmtime()) + '  ' + action + ':... ', end='')
                if action == 'sendData':
                    sendData()
                elif action == 'transpondersUpgrade':
                    transpondersUpgrade()
                else:
                    raise Exception("Nepodporovana funkce!");
                c = sqlS.cursor()
                c.execute("UPDATE cron SET last_run = strftime('%s', 'now') WHERE action_name = '" + action + "';")
                sqlS.commit()
                print('Ok')
            except Exception as e:
                #raise e
                print(sys.exc_info())
                print('Error!')
                sleep(10)

except KeyboardInterrupt:
    sqlS.close()
    rd.logout()
    print('End')
