
CREATE TABLE IF NOT EXISTS cron
(
  id serial NOT NULL,
  action_name text,
  last_run int DEFAULT 0, --as Unix Time, the number of seconds since 1970-01-01 00:00:00 UTC
  period int --Perioda v sec.
);

CREATE TABLE IF NOT EXISTS transponders
(
  id serial NOT NULL,
  first_name text,
  last_name text,
  "number" text,
  transponder text,
  enable boolean NOT NULL DEFAULT false
);

CREATE TABLE IF NOT EXISTS wevents
(   
  id INTEGER PRIMARY KEY,
  time int NOT NULL,
  place_id int NOT NULL,
  driver_transponder_id int,
  vehicle_transponder_ids int[],
  note text NOT NULL default '',
  brutto real NOT NULL,
  send_time int DEFAULT NULL
);
