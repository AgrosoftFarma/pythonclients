#!/usr/bin/env python3
'''
Created on 9. 7. 2017

@author: krupkaf
'''

import sqlite3
import sys
import configparser
import os
from time import sleep, gmtime, strftime

config = configparser.ConfigParser()
config.read(os.path.dirname(os.path.abspath(__file__)) + '/../../config/sklad.conf')

print('Start')

sqlS = sqlite3.connect(config.get('db', 'fileName'))

i = 0
while True:
    i = i + 1
    print(str(i) + ":")
    sleep(0.5)
    input()
    c = sqlS.cursor()
    c.execute("INSERT INTO wevents(time, place_id, driver_transponder_id, brutto) VALUES "
              + "(strftime('%s', 'now'), 1, (SELECT id FROM transponders ORDER BY random() LIMIT 1), (ABS(RANDOM()) % (30000 - 10000) + 10000))")
    sqlS.commit()
