#!/usr/bin/env python3
'''
Created on 9. 7. 2017

@author: krupkaf
'''

import sqlite3
import sys
import configparser
import os
from time import sleep
import serial
import doorman
import logging
import traceback

#***
# create logger
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter
formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s')
# add formatter to ch
ch.setFormatter(formatter)
# add ch to logger
logger.addHandler(ch)
# create file handler and set level to debug
fh = logging.FileHandler(os.path.dirname(os.path.realpath(__file__)) + '/scale.log')
fh.setLevel(logging.DEBUG)
# create formatter
formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s')
# add formatter to fh
fh.setFormatter(formatter)
# add fh to logger
logger.addHandler(fh)
#***

logger.info('Start')

config = configparser.ConfigParser()
config.read(os.path.dirname(os.path.abspath(__file__)) + '/../../config/sklad.conf')

placeIds = config.get('scale', 'placeIds').split()
readerAddress = config.get('modBus', 'readerAddrs').split()

if len(placeIds) != len(readerAddress):
    raise Exception(0, 'Chyba v konfigu. Položky modBus->readerAddr a store->placeIds')

sqlS = sqlite3.connect(config.get('db', 'fileName'), timeout = 30)
for sql in open(os.path.dirname(os.path.abspath(__file__)) + '/createDB.sql', 'r').read().split(';'):
    sqlS.execute(sql);

scaleType = config.get('scale', 'type')
if scaleType == 'CI200':
    scaleSer = serial.Serial(config.get('scale', 'port'), 9600, timeout=0.5)
elif scaleType == 'S20':
    scaleSer = serial.Serial(config.get('scale', 'port'), 2400, timeout=0.5)
else:
    raise ASapiException(0, 'Neni zadan typ vahy!')

doormans=[]
for (i, addr) in enumerate(readerAddress):
    doormans.append(doorman.Doorman(config.get('modBus', 'port'), int(addr)))

nStable = int(config.get('scale', 'nStable'))
#**************************************************************************************************************
def saveWeighing(driverTransponder, brutto, placeId):
    driverTransponder = str(driverTransponder).strip()
    driverTransponderId = sqlS.execute("SELECT id FROM transponders WHERE transponder = '" + driverTransponder + "' ORDER BY id desc LIMIT 1").fetchone()
    if driverTransponderId:
        driverTransponderId = driverTransponderId[0]
    else:
        driverTransponderId = None
    
    c = sqlS.cursor()
    c.execute("INSERT INTO wevents(time, place_id, driver_transponder_id, brutto, note) VALUES "
              + "(strftime('%s', 'now'),"
              + str(placeId) + ", "
              + (str(driverTransponderId) if driverTransponderId is not None else 'NULL') + ", "
              + str(brutto) + ","
              + "'" + (("Čip:" + str(driverTransponder)) if driverTransponderId is None else '' ) + "')")
    sqlS.commit()
#**************************************************************************************************************
def getWeight():    
    scaleSer.flushInput()
    lastVal = None
    val = None
    n = 0
    while True:
        line = scaleSer.readline()
        try:
            if scaleType == 'CI200':
                val = float(line.decode("utf-8").strip())
            elif scaleType == 'S20':
                val = float(line[1:13].decode("utf-8").strip())
            else:
                raise ASapiException(0, 'Neni zadan typ vahy!')
        except:
            val = None
        if val != lastVal or lastVal is None or val is None:
            lastVal = val
            n = 0
        else:
            n = n + 1
            if n >= nStable:
                break             
    return lastVal

#**************************************************************************************************************
while True:
    try:
        for (i, dm) in enumerate(doormans):
            dm.readTransponder()
        while True:
            sleep(0.05)
            for (i, dm) in enumerate(doormans):
                transponder = dm.readTransponder()
                if transponder is not None:
                    weight = getWeight()
                    logger.info(transponder + ': ' + str(weight) + ' kg; placeId:' + str(placeIds[i]))
                    saveWeighing(transponder, weight, placeIds[i])
                    dm.setRele(1, 1)
                    sleep(30)
                    dm.setRele(1, 0)
                    for (i1, dm1) in enumerate(doormans):
                        #Pokud byl behem vazeni a sviceni semaforu znovu nacten cip, zahodim jej.
                        #Pro jistotu ze vsech ctecek.
                        dm1.readTransponder()                    
                    break                    
    except KeyboardInterrupt as e:
        sqlS.close()
        logger.info('End')
        sys.exit()
    except BaseException as e:
        logger.error(traceback.format_exc())
sqlS.close()
logger.info('End, end')
