#!/usr/bin/env python3

import os
import sys
import configparser
import requests
import argparse


script_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(script_dir + os.sep + os.pardir)
# from asapi.common import Common


def get_config_file_name():
    script_name = os.path.splitext(os.path.basename(__file__))
    config_name = f'{script_dir}{os.sep}{script_name[0]}.conf'
    return config_name


def load_config(config_file_name) -> configparser.ConfigParser:
    config = configparser.ConfigParser()
    config.read(config_file_name)
    return config


def create_parser():
    parser = argparse.ArgumentParser(description='Data transfer between Agrosoft Skot and Afimilk')
    parser.add_argument('-s', '--to-skot', help='Export data from Afimilk to Skot', action='store_true')
    parser.add_argument('-a', '--to-afimilk', help='Import data to Afimilk from Skot', action='store_true')
    parser.add_argument('-f', '--full', help='Export and import data', action='store_true')
    return parser


def afimilk_to_skot(export_dir, server_url, user_name, user_password, company_id):
    # get all files from export dir of Afimilk
    list_of_files = [
        file for file in os.listdir(export_dir)
        if os.path.isfile(os.path.join(export_dir, file))
    ]

    # export data from Afimilk to server
    for file_name in list_of_files:
        file_path = os.path.join(export_dir, file_name)
        with open(file_path, 'rb') as file:
            data = file.read()
            # Some file has underscore in name. For example HerdMgmt_AnimalInfo_<date>.
            # For send to server is the name transformed to HerdMgmt/AnimalInfo/<date>.
            import_path = file_name.replace('_', '/')
            requests.post(f'{server_url}/import/{import_path}', json=data)
    pass


def skot_to_afimilk(import_dir, server_url, user_name, user_password, company_id):
    response = requests.post(f'{server_url}/export')
    response_json = response.json()

    for file_name in response_json['data']:
        file_path = os.path.join(import_dir, file_name)
        with open(file_path, 'wb') as file:
            data = requests.get(f'{server_url}/export/{file_name}')
            file.write(data.content)
    pass


def main():
    parser = create_parser()
    args = parser.parse_args()

    config_file_name = get_config_file_name()
    if not os.path.isfile(config_file_name):
        print("Configuration file not found!")
        print(f'Please create configuration file {config_file_name}')
        sys.exit(-1)

    config = load_config(config_file_name)

    # get config for Skot
    server_url = config.get('Agrosoft', 'url')
    user_name = config.get('Agrosoft', 'userName')
    user_password = config.get('Agrosoft', 'userPassword')
    company_id = config.get('Agrosoft', 'companyId', fallback=None)

    # get config for Afimilk
    export_dir = config.get('Afimilk', 'export')
    import_dir = config.get('Afimilk', 'import')

    if server_url is None or user_name is None or user_password is None or company_id is None:
        print("Configuration for Agrosoft not found!")
        missing = [key for key, value in [
            ('url', server_url),
            ('userName', user_name),
            ('userPassword', user_password),
            ('companyId', company_id)]
                   if value is None]
        print(f'Missing parameter: {missing}')
        sys.exit(-2)

    if export_dir is None or import_dir is None:
        print("Configuration for Afimilk not found!")
        missing = [key for key, value in [
            ('export', export_dir),
            ('import', import_dir)]
                   if value is None]
        print(f'Missing parameter: {missing}')
        sys.exit(-2)

    if args.to_skot or args.full:
        afimilk_to_skot(export_dir, server_url, user_name, user_password, company_id)

    if args.to_afimilk or args.full:
        skot_to_afimilk(import_dir, server_url, user_name, user_password, company_id)
    pass


if __name__ == "__main__":
    main()
